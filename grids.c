/******************************************************************
Author: Austin Lachance
Date: 10/28/16
Description: Defines the functions for implementing and manipulating
a linked list of Grid_nodes
******************************************************************/
#include <stdio.h>
#include <assert.h>
#include "grids.h"
#ifdef __APPLE__
#include <Python/Python.h>
#else
#include <Python.h>
#endif

#define DIE(msg) exit(fprintf(stderr, "%s\n", msg))


double *dt_ptr;
Grid_node *Parallel_grids[100] = {NULL};

// Set ∆t
void make_dt_ptr(PyHocObject* my_dt_ptr) {
    dt_ptr = my_dt_ptr -> u.px_;
}

// Add Flux * "flux" to a given grid *grid
void add_flux(Grid_node **grid, Flux* flux) {
    (*grid)->flux_list = flux;
}

// Make a new Grid_node given required Grid_node parameters
Grid_node *make_Grid(PyHocObject* my_states, int my_num_states_x, 
    int my_num_states_y, int my_num_states_z, double my_dc_x, double my_dc_y,
    double my_dc_z, double my_dx, double my_dy, double my_dz) {

    Grid_node *new_Grid = malloc(sizeof(Grid_node));
    assert(new_Grid);

    new_Grid->states = my_states->u.px_;

    new_Grid->size_x = my_num_states_x;
    new_Grid->size_y = my_num_states_y;
    new_Grid->size_z = my_num_states_z;

    new_Grid->flux_list = NULL;

    new_Grid->dc_x = my_dc_x;
    new_Grid->dc_y = my_dc_y;
    new_Grid->dc_z = my_dc_z;

    new_Grid->dx = my_dx;
    new_Grid->dy = my_dy;
    new_Grid->dz = my_dz;
    new_Grid->old_states = (double *) malloc(sizeof(double) * new_Grid->size_x * 
        new_Grid->size_y * new_Grid->size_z);

    assert(new_Grid->old_states);

    new_Grid->next = NULL;

    return new_Grid;
}


// Insert a Grid_node "new_Grid" into the list located at grid_list_index in Parallel_grids
void insert(int grid_list_index, PyHocObject* my_states, int my_num_states_x, 
    int my_num_states_y, int my_num_states_z, double my_dc_x, double my_dc_y,
    double my_dc_z, double my_dx, double my_dy, double my_dz) {

    printf("INSERTING\n");

    Grid_node *new_Grid = make_Grid(my_states, my_num_states_x, my_num_states_y, 
            my_num_states_z, my_dc_x, my_dc_y, my_dc_z, my_dx, my_dy, my_dz);

    Grid_node **head = &(Parallel_grids[grid_list_index]);
    Grid_node *save;

    if(!(*head)) {
        *head = new_Grid;
        save = *head;
    }
    else {
        save = *head;
        Grid_node *end = *head;
        while(end->next != NULL) {
            end = end->next;
        }
        end->next = new_Grid;
    }

    while(save != NULL) {
        printf("SIZE X: %d SIZE Y: %d SIZE Z: %d\n", save->size_x, save->size_y, save->size_z);
        save = save->next;
    }

}
    

// Free a single Grid_node
void free_Grid(Grid_node *grid) {
    free(grid->states);
    free(grid->old_states);
    free(grid->flux_list);
    free(grid);
}

// Insert a Grid_node into the linked list
/*void insert(Grid_node **head, Grid_node *new_Grid) {
    if(!(*head)) {
        *head = new_Grid;
        return;
    }

    Grid_node *end = *head;
    while(end->next != NULL) {
        end = end->next;
    }
    end->next = new_Grid;
}*/

// Delete a specific Grid_node from the list
int delete(Grid_node **head, Grid_node *find) {
    if(*head == find) {
        Grid_node *temp = *head;
        *head = (*head)->next;
        free_Grid(temp);
        return 1;
    }
    Grid_node *temp = *head;
    while(temp->next != find) {
        temp = temp->next;
    }
    if(!temp) return 0;
    Grid_node *delete_me = temp->next;
    temp->next = delete_me->next;
    free_Grid(delete_me);
    return 1;
}


// Destroy the list located at list_index and free all memory
void empty_list(int list_index) {
    Grid_node **head = &(Parallel_grids[list_index]);
    while(*head != NULL) {
        Grid_node *old_head = *head;
        *head = (*head)->next;
        free_Grid(old_head);
    }
}
