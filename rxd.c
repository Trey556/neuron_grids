#include <stdio.h>
#include <assert.h>
#include "grids.h"
#ifdef __APPLE__
#include <Python/Python.h>
#else
#include <Python.h>
#endif

#define DIE(msg) exit(fprintf(stderr, "%s\n", msg))


typedef void (*fptr)(void);


void update_boundaries_x(int i, int j, int k, int dj, int dk, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states);

void update_boundaries_y(int i, int j, int k, int di, int dk, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states);

void update_boundaries_z(int i, int j, int k, int di, int dj, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states);

void _fadvance_helper(double **ref_states, double **ref_old_states,
    int num_states_x, int num_states_y, int num_states_z, double dc_x,
    double dc_y, double dc_z, double dx, double dy, double dz);



int find(int x, int y, int z, int size_y, int size_z) {
    int index = z + y * size_z + x * size_z * size_y;
    return index;

}

/*void setup_solver(PyHocObject* my_states, PyHocObject* my_dt_ptr, int my_num_states_x,
 int my_num_states_y, int my_num_states_z, double dc_x, double dc_y, double dc_z, 
 double my_dx, double my_dy, double my_dz) {
    states = my_states -> u.px_;
    num_states_x = my_num_states_x;
    num_states_y = my_num_states_y;
    num_states_z = my_num_states_z;
    diffusion_constant_x = dc_x;
    diffusion_constant_y = dc_y;
    diffusion_constant_z = dc_z;
    dt_ptr = my_dt_ptr -> u.px_;
    dx = my_dx;
    dy = my_dy;
    dz = my_dz;
    old_states = (double *) malloc(sizeof(double) * num_states_x * num_states_y
     * num_states_z);
}*/

void test(int num) {
    // states = my_states -> u.px_;
    // printf("(9, 4, 7) = %f\n", states[find(9, 4, 7, 13, 17)]);
    // printf("TEST: %d\n", num);
}

void _fadvance(void) {
    printf("fadvance\n");
    Grid_node *temp = Parallel_grids[0];
    while(temp != NULL) {
        printf("Advancing Grid %dx%dx%d\n", temp->size_x, temp->size_y, temp->size_z);
        _fadvance_helper(&(temp->states), &(temp->old_states), temp->size_x,
            temp->size_y, temp->size_z, temp->dc_x, temp->dc_y, temp->dc_z,
            temp->dx, temp->dy, temp->dz);

        temp = temp->next;
    }
}

void _fadvance_helper(double **ref_states, double **ref_old_states,
    int num_states_x, int num_states_y, int num_states_z, double dc_x,
    double dc_y, double dc_z, double dx, double dy, double dz) {

    double *states = *ref_states;
    double *old_states = *ref_old_states;
    int i, j, k, stop_i, stop_j, stop_k;
    double rate_x = dc_x * (*dt_ptr) / (dx * dx);
    double rate_y = dc_y * (*dt_ptr) / (dy * dy);
    double rate_z = dc_z * (*dt_ptr) / (dz * dz);
    
    /* make a copy of the states to work from */
    int total_states = num_states_x * num_states_y * num_states_z;
    for (i = 0; i < total_states; i++) {
        old_states[i] = states[i];
    }

    /* Euler advance x, y, z (all internal points) */
    stop_i = num_states_x - 1;
    stop_j = num_states_y - 1;
    stop_k = num_states_z - 1;
    for (i = 1; i < stop_i; i++) {
        for(j = 1; j < stop_j; j++) {
            for(k = 1; k < stop_k; k++) {
                int index = find(i, j, k, num_states_y, num_states_z);
                int prev_i = find(i-1, j, k, num_states_y, num_states_z);
                int prev_j = find(i, j-1, k, num_states_y, num_states_z);
                int prev_k = find(i, j, k-1, num_states_y, num_states_z);
                int next_i = find(i+1, j, k, num_states_y, num_states_z);
                int next_j = find(i, j+1, k, num_states_y, num_states_z);
                int next_k = find(i, j, k+1, num_states_y, num_states_z);

                states[index] += rate_x * (old_states[prev_i] - 2 * 
                    old_states[index] + old_states[next_i]);

                states[index] += rate_y * (old_states[prev_j] - 2 * 
                    old_states[index] + old_states[next_j]);

                states[index] += rate_z * (old_states[prev_k] - 2 * 
                    old_states[index] + old_states[next_k]);
            }   
        }
    }

    /* boundary conditions for Z faces */
    for(i = 1; i < stop_i; i++) {
        for(j = 1; j < stop_j; j++) {
            int index = find(i, j, 0, num_states_y, num_states_z);
            int prev_i = find(i-1, j, 0, num_states_y, num_states_z);
            int prev_j = find(i, j-1, 0, num_states_y, num_states_z);
            int next_i = find(i+1, j, 0, num_states_y, num_states_z);
            int next_j = find(i, j+1, 0, num_states_y, num_states_z);
            int next_k = find(i, j, 1, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[prev_i] - 2 * 
                old_states[index] + old_states[next_i]);

            states[index] += rate_y * (old_states[prev_j] - 2 * 
                old_states[index] + old_states[next_j]);

            states[index] += rate_z * (old_states[next_k] - old_states[index]);

            index = find(i, j, stop_k, num_states_y, num_states_z);
            prev_i = find(i-1, j, stop_k, num_states_y, num_states_z);
            prev_j = find(i, j-1, stop_k, num_states_y, num_states_z);
            next_i = find(i+1, j, stop_k, num_states_y, num_states_z);
            next_j = find(i, j+1, stop_k, num_states_y, num_states_z);
            next_k = find(i, j, stop_k-1, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[prev_i] - 2 * 
                old_states[index] + old_states[next_i]);

            states[index] += rate_y * (old_states[prev_j] - 2 * 
                old_states[index] + old_states[next_j]);

            states[index] += rate_z * (old_states[next_k] - old_states[index]);
        }   
    }   

    /* boundary conditions for X faces */
    for(j = 1; j < stop_j; j++) {
        for(k = 1; k < stop_k; k++) {
            int index = find(0, j, k, num_states_y, num_states_z);
            int prev_j = find(0, j-1, k, num_states_y, num_states_z);
            int prev_k = find(0, j, k-1, num_states_y, num_states_z);
            int next_j = find(0, j+1, k, num_states_y, num_states_z);
            int next_k = find(0, j, k+1, num_states_y, num_states_z);
            int next_i = find(1, j, k, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[next_i] - old_states[index]);

            states[index] += rate_y * (old_states[prev_j] - 2 * 
                old_states[index] + old_states[next_j]);

            states[index] += rate_z * (old_states[prev_k] - 2 * 
                old_states[index] + old_states[next_k]);

            index = find(stop_i, j, k, num_states_y, num_states_z);
            prev_j = find(stop_i, j-1, k, num_states_y, num_states_z);
            prev_k = find(stop_i, j, k-1, num_states_y, num_states_z);
            next_j = find(stop_i, j+1, k, num_states_y, num_states_z);
            next_k = find(stop_i, j, k+1, num_states_y, num_states_z);
            next_i = find(stop_i-1, j, k, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[next_i] - old_states[index]);

            states[index] += rate_y * (old_states[prev_j] - 2 * 
                old_states[index] + old_states[next_j]);

            states[index] += rate_z * (old_states[prev_k] - 2 * 
                old_states[index] + old_states[next_k]);
        }   
    }   

    /* boundary conditions for Y faces */
    for(i = 1; i < stop_i; i++) {
        for(k = 1; k < stop_k; k++) {
            int index = find(i, 0, k, num_states_y, num_states_z);
            int prev_i = find(i-1, 0, k, num_states_y, num_states_z);
            int prev_k = find(i, 0, k-1, num_states_y, num_states_z);
            int next_i = find(i+1, 0, k, num_states_y, num_states_z);
            int next_k = find(i, 0, k+1, num_states_y, num_states_z);
            int next_j = find(i, 1, k, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[prev_i] - 2 * 
                old_states[index] + old_states[next_i]);

            states[index] += rate_y * (old_states[next_j] - old_states[index]);

            states[index] += rate_z * (old_states[prev_k] - 2 * 
                old_states[index] + old_states[next_k]);

            index = find(i, stop_j, k, num_states_y, num_states_z);
            prev_i = find(i-1, stop_j, k, num_states_y, num_states_z);
            prev_k = find(i, stop_j, k-1, num_states_y, num_states_z);
            next_i = find(i+1, stop_j, k, num_states_y, num_states_z);
            next_k = find(i, stop_j, k+1, num_states_y, num_states_z);
            next_j = find(i, stop_j-1, k, num_states_y, num_states_z);

            states[index] += rate_x * (old_states[prev_i] - 2 * 
                old_states[index] + old_states[next_i]);

            states[index] += rate_y * (old_states[next_j] - old_states[index]);

            states[index] += rate_z * (old_states[prev_k] - 2 * 
                old_states[index] + old_states[prev_k]);
        }   
    }

    /* boundary conditions for edges */
    for(i = 1; i < stop_i; i++) {
        update_boundaries_x(i, 0, 0, 1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_x(i, stop_j, 0, -1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_x(i, 0, stop_k, 1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_x(i, stop_j, stop_k, -1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
    }
    for(j = 1; j < stop_j; j++) {
        update_boundaries_y(0, j, 0, 1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_y(stop_i, j, 0, -1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_y(0, j, stop_k, 1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_y(stop_i, j, stop_k, -1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
    }
    for(k = 1; k < stop_k; k++) {
        update_boundaries_z(0, 0, k, 1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_z(stop_i, 0, k, -1, 1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_z(0, stop_j, k, 1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
        update_boundaries_z(stop_i, stop_j, k, -1, -1, rate_x, rate_y, rate_z, num_states_x, num_states_y, num_states_z, &states, &old_states);
    }

    /* boundary conditions for corners */
    int next_i, next_j, next_k;
    for(i = 0; i <= stop_i; i += stop_i) {
        for(j = 0; j <= stop_j; j += stop_j) {
            for(k = 0; k <= stop_k; k += stop_k) {
                int corner = find(i, j, k, num_states_y, num_states_z);
                if(!i) next_i = find(i+1, j, k, num_states_y, num_states_z);
                else next_i = find(i-1, j, k, num_states_y, num_states_z);
                if(!j) next_j = find(i, j+1, k, num_states_y, num_states_z);
                else next_j = find(i, j-1, k, num_states_y, num_states_z);
                if(!k) next_k = find(i, j, k+1, num_states_y, num_states_z);
                else next_k = find(i, j, k-1, num_states_y, num_states_z);
                states[corner] += rate_x * (old_states[next_i] - old_states[corner]);
                states[corner] += rate_y * (old_states[next_j] - old_states[corner]);
                states[corner] += rate_z * (old_states[next_k] - old_states[corner]);
            }
        }
    }

}

/* update x-dimension edge points */
/* function takes in combinations of j and k to represent the edges as well as
1 or -1 for dj and dk to indicate which adjacent location is still in bounds */
void update_boundaries_x(int i, int j, int k, int dj, int dk, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states) {

    double *states = *ref_states;
    double *old_states = *ref_old_states;
    int edge = find(i, j, k, num_states_y, num_states_z);
    int prev_i = find(i-1, j, k, num_states_y, num_states_z);
    int next_i = find(i+1, j, k, num_states_y, num_states_z);
    int next_j = find(i, j + dj, k, num_states_y, num_states_z);
    int next_k = find(i, j, k + dk, num_states_y, num_states_z);

    states[edge] += rate_x * (old_states[prev_i] - 2 * old_states[edge]
     + old_states[next_i]);
    states[edge] += rate_y * (old_states[next_j] - old_states[edge]);
    states[edge] += rate_z * (old_states[next_k] - old_states[edge]);
}

/* update y-dimension edge points */
void update_boundaries_y(int i, int j, int k, int di, int dk, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states) {

    double *states = *ref_states;
    double *old_states = *ref_old_states;
    int edge = find(i, j, k, num_states_y, num_states_z);
    int prev_j = find(i, j-1, k, num_states_y, num_states_z);
    int next_j = find(i, j+1, k, num_states_y, num_states_z);
    int next_i = find(i + di, j, k, num_states_y, num_states_z);
    int next_k = find(i, j, k + dk, num_states_y, num_states_z);

    states[edge] += rate_x * (old_states[next_i] - old_states[edge]);
    states[edge] += rate_y * (old_states[prev_j] - 2 * old_states[edge]
     + old_states[next_j]);
    states[edge] += rate_z * (old_states[next_k] - old_states[edge]);
}

/* update z-dimension edge points */
void update_boundaries_z(int i, int j, int k, int di, int dj, double rate_x,
 double rate_y, double rate_z, int num_states_x, int num_states_y, int num_states_z,
 double **ref_states, double **ref_old_states) {

    double *states = *ref_states;
    double *old_states = *ref_old_states;
    int edge = find(i, j, k, num_states_y, num_states_z);
    int prev_k = find(i, j, k-1, num_states_y, num_states_z);
    int next_k = find(i, j, k+1, num_states_y, num_states_z);
    int next_i = find(i + di, j, k, num_states_y, num_states_z);
    int next_j = find(i, j + dj, k, num_states_y, num_states_z);

    states[edge] += rate_x * (old_states[next_i] - old_states[edge]);
    states[edge] += rate_y * (old_states[next_j] - old_states[edge]);
    states[edge] += rate_z * (old_states[prev_k] - 2 * old_states[edge] + 
        old_states[next_k]);
}


int rxd_nonvint_block(int method, int size, double* p1, double* p2, int thread_id) {
    switch (method) {
        case 0:
            _setup();
            break;
        case 1:
            _initialize();
            break;
        case 2:
            /* compute outward current to be subtracted from rhs */
            break;
        case 3:
            /* conductance to be added to d */
            break;
        case 4:
            /* fixed step solve */
            _fadvance();
            break;
        case 5:
            /* ode_count */
            break;
        case 6:
            /* ode_reinit(y) */
            break;
        case 7:
            /* ode_fun(t, y, ydot); from t and y determine ydot */
            break;
        case 8:
            /* ode_solve(dt, t, b, y); solve mx=b replace b with x */
        case 9:
            /* ode_jacobian(dt, t, ypred, fpred); optionally prepare jacobian for fast ode_solve */
            break;
        case 10:
            /* ode_abs_tol(y_abs_tolerance); fill with cvode.atol() * scalefactor */
            break;
        default:
            printf("Unknown rxd_nonvint_block call: %d\n", method);
            break;
    }
	/* printf("method=%d, size=%d, thread_id=%d\n", method, size, thread_id);	 */
}

void set_setup(fptr setup_fn) {
	_setup = setup_fn;
}

void set_initialize(fptr initialize_fn) {
	_initialize = initialize_fn;
    printf("SET INITIALIZE\n");
}

/* verbatim from nrnoc/ldifus.c; included as a copy for convenience
   TODO: use this instead of Euler
   TODO: remove the duplication
 */
void nrn_tree_solve(double* a, double* d, double* b, double* rhs, int* pindex, int n) {
    /*
        treesolver
        
        a      - above the diagonal 
        d      - diagonal
        b      - below the diagonal
        rhs    - right hand side, which is changed to the result
        pindex - parent indices
        n      - number of states
    */

    int i;

	/* triang */
	for (i = n - 1; i > 0; --i) {
		int pin = pindex[i];
		if (pin > -1) {
			double p;
			p = a[i] / d[i];
			d[pin] -= p * b[i];
			rhs[pin] -= p * rhs[i];
		}
	}
	/* bksub */
	for (i = 0; i < n; ++i) {
		int pin = pindex[i];
		if (pin > -1) {
			rhs[i] -= b[i] * rhs[pin];
		}
		rhs[i] /= d[i];
	}
}

// double* states1;
// double* states2;
// double* states3;
int num_states;



// void fadvance(void) {
// }
