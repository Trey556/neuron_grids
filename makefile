#gcc -I/usr/include/python2.7 -lpython2.7 -shared -o rxd.so -fPIC rxd.c 

# Makefile for files: rxd.c , grids.c , grids.h

CC = gcc
CFLAGS = -g3 -std=c99 -pedantic -Wall

rxd: rxd.o grids.o
	${CC} ${CFLAGS} -I/usr/include/python2.7 -lpython2.7 -shared grids.o -o rxd.so -fPIC rxd.c 

