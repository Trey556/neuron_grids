from neuron import h
import rxdfast as rxd
from matplotlib import pyplot
#from volume_slicer import VolumeSlicer
h.load_file('stdrun.hoc')

h.finitialize()

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 4, i + 1)
    if i == 8: i = rxd.size_x2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[i,:, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(7):
    pyplot.subplot(3, 4, i + 1)
    if i == 7: i = rxd.size_2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[:, i, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(11):
    pyplot.subplot(3, 4, i + 1)
    if i == 11: i = rxd.size_z2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[:,:, i], interpolation='nearest', vmin=0, vmax=1)



print 'ready to do an advance?'
for i in xrange(100):
    h.fadvance()
print 'well?'

#m = VolumeSlicer(data=rxd.states.as_numpy().reshape(rxd.size_x, rxd.size_y, rxd.size_z))
#m.configure_traits()

pyplot.figure()

for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_x1 - 1
    pyplot.imshow(rxd.states1.as_numpy().reshape(rxd.size_x1, rxd.size_y1, rxd.size_z1)[i,:, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_y1 - 1
    pyplot.imshow(rxd.states1.as_numpy().reshape(rxd.size_x1, rxd.size_y1, rxd.size_z1)[:, i, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_z1 - 1
    pyplot.imshow(rxd.states1.as_numpy().reshape(rxd.size_x1, rxd.size_y1, rxd.size_z1)[:,:, i], interpolation='nearest', vmin=0, vmax=1)


pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_x2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[i,:, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_y2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[:, i, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_z2 - 1
    pyplot.imshow(rxd.states2.as_numpy().reshape(rxd.size_x2, rxd.size_y2, rxd.size_z2)[:,:, i], interpolation='nearest', vmin=0, vmax=1)


"""

for i in xrange(17):
    pyplot.subplot(4, 5, i + 1)
    if i == 17: i = rxd.size_x3 - 1
    pyplot.imshow(rxd.states3.as_numpy().reshape(rxd.size_x3, rxd.size_y3, rxd.size_z3)[i,:, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(5):
    pyplot.subplot(3, 4, i + 1)
    if i == 8: i = rxd.size_y3 - 1
    pyplot.imshow(rxd.states3.as_numpy().reshape(rxd.size_x3, rxd.size_y3, rxd.size_z3)[:, i, :], interpolation='nearest', vmin=0, vmax=1)

pyplot.figure()
for i in xrange(9):
    pyplot.subplot(3, 3, i + 1)
    if i == 8: i = rxd.size_z3 - 1
    pyplot.imshow(rxd.states3.as_numpy().reshape(rxd.size_x3, rxd.size_y3, rxd.size_z3)[:,:, i], interpolation='nearest', vmin=0, vmax=1)
"""
pyplot.show()




# sec = h.Section()
# sec.nseg = 100
# sec.L = 100

# r = rxd.Region([sec])
# ca = rxd.Species(r, dc_x=1, dc_y=1, dc_z=1, initial=lambda node: 1 if 0.4 < node.x < 0.6 else 0)

# def plot_it():
#     pyplot.plot([seg.x * sec.L for seg in sec], ca.states)

# h.finitialize()

# for i in xrange(5):
#     h.continuerun(i * 25)
#     plot_it()

# pyplot.xlim([0, sec.L])
# pyplot.ylim([0, 1])
# pyplot.show()
