"""
rxdfast module

interconnection between Python and C for NEURON rxd v2
"""

from neuron import nrn_dll_sym, nrn_dll, h
import ctypes

set_nonvint_block = nrn_dll_sym('set_nonvint_block')
nrn = nrn_dll()
dll = ctypes.cdll['./rxd.so']

fptr_prototype = ctypes.CFUNCTYPE(None)

set_nonvint_block(dll.rxd_nonvint_block)

set_setup = dll.set_setup
set_setup.argtypes = [fptr_prototype]
set_initialize = dll.set_initialize
set_initialize.argtypes = [fptr_prototype]

#setup_solver = dll.setup_solver
#setup_solver.argtypes = [ctypes.py_object, ctypes.py_object, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double]

test = dll.test
test.argtypes = [ctypes.c_int]

insert = dll.insert
insert.argtypes = [ctypes.c_int, ctypes.py_object, ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double]

test = dll.test
test.argtypes = [ctypes.py_object]

make_dt_ptr = dll.make_dt_ptr
make_dt_ptr.argtypes = [ctypes.py_object]

#states = None

region_sec = None
#diffusion_constant_x = 1 # TODO: these are per grid
#diffusion_constant_y = 1
#diffusion_constant_z = 1
#states = None
my_initializer = None

"""
class Node:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

def Region(secs):
    global region_sec
    assert(len(secs) == 1)
    region_sec = secs

class Species:
    def __init__(self, region, dc_x=0, dc_y=0, dc_z=0, initial=None):
        global diffusion_constant_x, diffusion_constant_y, diffusion_constant_z, my_initializer
        assert(region is None)
        diffusion_constant_x = dc_x
        diffusion_constant_y = dc_y
        diffusion_constant_z = dc_z
        my_initializer = initial
    @property
    def states(self):
        return states


Class Grid:
    def __init__(self, size_x, size_y, size_z):
        self.s

"""

has_setup = False
def do_setup():
    global states1, size_x1, size_y1, size_z1, has_setup
    global states2, size_x2, size_y2, size_z2
    global states3, size_x3, size_y3, size_z3
    has_setup = True
    size_x1, size_y1, size_z1 = 11, 13, 17
    size_x2, size_y2, size_z2 = 11, 13, 17
    size_x3, size_y3, size_z3 = 18, 5, 9
    states1 = h.Vector(size_x1 * size_y1 * size_z1)
    states2 = h.Vector(size_x2 * size_y2 * size_z2)
    states3 = h.Vector(size_x3 * size_y3 * size_z3)
    # v2 = states.as_numpy()
    # v3 = v2.reshape(11, 13, 17)
    # v3[9,4,7] = 9
    # print v3[9,4,7]
    # print v3
    # print v2.nonzero()
    #test(3)

    #setup_solver(states._ref_x[0], h._ref_dt, size_x, size_y, size_z,
     #diffusion_constant_x, diffusion_constant_y, diffusion_constant_z, 1.0, 1.0, 1.0)
    print "DO SETUP!"
    make_dt_ptr(h._ref_dt);
    insert(0, states1._ref_x[0], size_x1, size_y1, size_z1, 1.0, 1.2, 1.3, 1.0, 1.0, 1.0);
    insert(0, states2._ref_x[0], size_x2, size_y2, size_z2, 2.0, 2.4, 2.6, 1.0, 1.0, 1.0);
    insert(0, states3._ref_x[0], size_x3, size_y3, size_z3, 1.0, 1.2, 1.3, 1.0, 1.0, 1.0);
    print "Did we insert?"
    # setup_solver(states._ref_x[0], h._ref_dt, len(states), diffusion_constant, float(0))
    # dll.test(states._ref_x[0])

def do_initialize():
    print 'do_initialize called', has_setup
    if has_setup:
        """handle initialization at finitialize time"""
        my_states1 = states1.as_numpy().reshape(size_x1, size_y1, size_z1)
        my_states1[:, :, :] = 0
        my_states1[4:7, :, :] = 1

        my_states2 = states2.as_numpy().reshape(size_x2, size_y2, size_z2)
        my_states2[:, :, :] = 0
        my_states2[4:7, :, :] = 1

        my_states3 = states3.as_numpy().reshape(size_x3, size_y3, size_z3)
        my_states3[:, :, :] = 0
        my_states3[:, :, :] = 1

# register the Python callbacks


do_setup_fptr = fptr_prototype(do_setup)
do_initialize_fptr = fptr_prototype(do_initialize)
set_setup(do_setup_fptr)
set_initialize(do_initialize_fptr)
